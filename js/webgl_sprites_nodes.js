//import nodes for SpriteNodesLetters class
import './nodes/THREE.Nodes.js';
import './loaders/NodeMaterialLoader.js';

var osc = require("osc");
var { allowUnsafeEval } = require('loophole')
var SDFShader = require('./shaders/sdf')
//bmfont for creating whole word text strings
var loadFont = require('load-bmfont')
const createGeometry = allowUnsafeEval(() => require('three-bmfont-text'));

// var jsonfile = "./data/190106_wordsignal/newAlign.json";
// var jsonfile = "./data/solfege/newAlign.json";
var jsonfile = "./data/bodies/newAlign.json";

// var $ = require("jquery");

//how to construct a word from individual letters
//how to loop through the text over time

var container = document.getElementById('container');

var renderer, scene, camera, clock = new THREE.Clock(), fov = 50;
var spriteLetters = new SpriteNodesLetters();
var controls;
var mesh;
var textmesh;
var group;
var oscMsg;
var frameCount = 0;
var letterIndex = 0;
var textData;

var textGeometry;

var wordTimer;
var currentWord, currentPhone;
var currentWordGroup; //group of letters that form a word
var wordIndex, phoneDur = 500, phoneStartSample, numPhones, phoneIndex, totalPhoneIndex = 0;
var durationString, sampleString;
window.addEventListener('load', init);


//--parse json file
// jsonData.transcript = whole text

var request = new XMLHttpRequest();
request.open("GET", jsonfile, false);
request.send(null)
var jsonData = JSON.parse(request.responseText);
//iterate to word - num phonemes? - iterate to phoneme - send to osc - wait duration - next phoneme
//make function to return indexed word
var getIPAddresses = function () {
	var os = require("os"),
		interfaces = os.networkInterfaces(),
		ipAddresses = [];

	for (var deviceName in interfaces) {
		var addresses = interfaces[deviceName];
		for (var i = 0; i < addresses.length; i++) {
			var addressInfo = addresses[i];
			if (addressInfo.family === "IPv4" && !addressInfo.internal) {
				ipAddresses.push(addressInfo.address);
			}
		}
	}

	return ipAddresses;
};

///---init osc
var udpPort = new osc.UDPPort({
	// This is the port we're listening on.
	localAddress: "127.0.0.1",
	localPort: 57121,

	// This is where sclang is listening for OSC messages.
	remoteAddress: "127.0.0.1",
	remotePort: 57120,
	metadata: true
});


udpPort.on("ready", function () {
	var ipAddresses = getIPAddresses();
	console.log("Listening for OSC over UDP.");
	ipAddresses.forEach(function (address) {
		console.log(" Host:", address + ", Port:", udpPort.options.localPort);
	});
	console.log("Broadcasting OSC over UDP to", udpPort.options.remoteAddress + ", Port:", udpPort.options.remotePort);
});

udpPort.on("message", function (oscMessage) {
	var oscAddress = oscMessage.address;
	var msg = oscMessage.args[0];
	for (var key in msg) {
		var oscValue = msg[key];
	}
	if (oscAddress == "word")
		console.log(oscAddress);
	//upon request from SC, send back the next word, 
	//complete with phoneme and startsample data
	updateWord();
	console.log("received osc message. address: " + oscAddress + " value: " + oscValue);
});

// Open the socket.
udpPort.open();





function init() {

	//
	// Renderer / Controls
	//
	window.eval = global.eval = function () {
		throw new Error(`Sorry, this app does not support window.eval().`)
	}

	renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true, transparent: true });

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	container.appendChild(renderer.domElement);

	scene = new THREE.Scene();
	scene.background = new THREE.Color(0xD3D3D3);
	// scene.fog = new THREE.Fog( 0x0000FF, 70, 150 );

	camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, 1, 1000);
	camera.position.z = 100;
	camera.target = new THREE.Vector3();

	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.minDistance = 50;
	controls.maxDistance = 200;

	//add word text

	loadFont('./fonts/Helvetica.fnt', function (err, font) {
		// create a geometry of packed bitmap glyphs, 
		// word wrapped to 300px and right-aligned
		textGeometry = createGeometry({
			width:500,
			align: 'left',
			font: font
		})

		// change text and other options as desired
		// the options sepcified in constructor will
		// be used as defaults

		// the resulting layout has metrics and bounds
		console.log(textGeometry.layout.height)
		console.log(textGeometry.layout.descender)

		// the texture atlas containing our glyphs
		var textureLoader = new THREE.TextureLoader();
		textureLoader.load('./fonts/Helvetica.png', function (texture) {
			// we can use a simple ThreeJS material
			// var material = new THREE.MeshBasicMaterial({
			// 	map: texture,
			// 	transparent: true,
			// })
			// 	color: 0x000000

			var maxAni = renderer.getMaxAnisotropy()

			// setup our texture with some nice mipmapping etc
			texture.needsUpdate = true
			texture.minFilter = THREE.LinearMipMapLinearFilter
			texture.magFilter = THREE.LinearFilter
			texture.generateMipmaps = true
			texture.anisotropy = maxAni

			// here we use 'three-bmfont-text/shaders/sdf'
			// to help us build a shader material
			var material = new THREE.RawShaderMaterial(SDFShader({
				map: texture,
				side: THREE.DoubleSide,
				transparent: true,
				color: 'rgb(0, 0, 0)'
			}))

			textmesh = new THREE.Mesh(textGeometry, material)
			textmesh.rotateX(Math.PI);

			// scale it down so it fits in our 3D units
			var textAnchor = new THREE.Object3D()
			textAnchor.scale.multiplyScalar(0.2)
			textAnchor.position.x -= 25;
			textAnchor.add(textmesh)


			// textmesh.rotateY(Math.PI);
			scene.add(textAnchor);


			// function updateWord() {
			// 	if (phoneIndex >= numPhones || wordIndex == null) {
			// 		currentWord = getNextWord();
			// 		textGeometry.update(currentWord)

			// 	} else {
			// 		currentPhone = getNextPhone();
			// 		// sendOscPhonemeIndex(currentPhone)
			// 		sendOscPhonemeIndex(totalPhoneIndex);
			// 	}
			// }

			// timer function
			// (function repeat() {
			// 	updateWord();
			// 	wordTimer = setTimeout(repeat, phoneDur * 2);
			// })();

		})
		console.log("done........");



	})


	// spriteLetters.init();
	// scene.add(spriteLetters.getGroup());


	function onDocumentKeyPress(event) {

		var keyCode = event.which;
		console.log("event.key:  " + event.key)
		// spriteLetters.TriggerLetter(event.key);

	}

	//
	// Events
	//

	window.addEventListener('resize', onWindowResize, false);
	document.addEventListener('keypress', onDocumentKeyPress, false);

	onWindowResize();
	animate();

}

function getNextWord() {
	var totalWords = Object.keys(jsonData.words).length;
	if (wordIndex == null || wordIndex >= totalWords-1)
		wordIndex = 0;
	else
		wordIndex++;
	//in case we hit a not-found-in-audio word
	if (jsonData.words[wordIndex].phones == null) {
		numPhones = 0;
		return "";
	}

	numPhones = jsonData.words[wordIndex].phones.length;
	durationString = [];
	sampleString = [];
	for (var i = 0; i < numPhones; i++) {
		console.log("i/numPhones " + i+"/"+numPhones);
		durationString = durationString.concat(jsonData.words[wordIndex].phones[i].duration);
		sampleString = sampleString.concat(jsonData.words[wordIndex].phones[i].start_sample);
	}
	durationString.toString();
	sampleString.toString();

	phoneIndex = 0;
	console.log(jsonData.words[wordIndex].word);
	return jsonData.words[wordIndex].word;

}

function getNextPhone() {
	if (phoneIndex == null)
		phoneIndex = 0;

	phoneDur = parseFloat(jsonData.words[wordIndex].phones[phoneIndex].duration) * 1000;
	phoneStartSample = jsonData.words[wordIndex].phones[phoneIndex].start_sample;
	//leave out the phoneme encodings
	var phoneme = jsonData.words[wordIndex].phones[phoneIndex].phone.split("_")[0];
	phoneIndex++;
	totalPhoneIndex++;
	console.log(phoneme);
	return phoneme;
}

function onWindowResize() {

	var width = window.innerWidth, height = window.innerHeight;

	camera.aspect = width / height;
	camera.updateProjectionMatrix();

	renderer.setSize(width, height);

}

function animate() {

	renderer.render(scene, camera);

	requestAnimationFrame(animate);
	frameCount++;
}


function sendOscPhoneme(letter) {
	console.log("osc: " + letter)
	var msg = {
		address: "/phoneme",
		args: [
			{
				type: "s",
				value: letter
			}
		]
	};
	console.log("Sending message", msg.address, msg.args, "to", udpPort.options.remoteAddress + ":" + udpPort.options.remotePort);
	udpPort.send(msg);

}
function updateWord() {
	console.log("updateWord");
	if (phoneIndex >= numPhones-1 || wordIndex == null) {
		currentWord = getNextWord();
		textGeometry.update(currentWord)

		sendOscWordData(durationString, sampleString);
	} else {
		currentPhone = getNextPhone();
		// sendOscPhonemeIndex(currentPhone)
		// sendOscPhonemeIndex(totalPhoneIndex);
	}
}


function sendOscPhonemeIndex(phonemeIndex) {
	console.log("oscindex: " + phonemeIndex)
	var msg = {
		address: "/phonemeIndex",
		args: [
			{
				type: "i",
				value: phonemeIndex
			}
		]
	};

	console.log("Sending message", msg.address, msg.args, "to", udpPort.options.remoteAddress + ":" + udpPort.options.remotePort);
	udpPort.send(msg);

}

//iterate to word - num phonemes? - iterate to phoneme - send to osc - wait duration - next phoneme

//phoneme list are sent as comma separated string, same with sample list 
function sendOscWordData(durationString, sampleString) {
	console.log("WordData: " + durationString + " " + sampleString)
	var msg = {
		address: "/WordData",
		args: [
			{
				type: "s",
				value: durationString
			},
			{
				type: "s",
				value: sampleString
			}
		]
	};

	console.log("Sending message", msg.address, msg.args, "to", udpPort.options.remoteAddress + ":" + udpPort.options.remotePort);
	udpPort.send(msg);

}
