

function SpriteNodesLetters() {
    this.letterIndex = 0;
    this.frame = new THREE.NodeFrame();
    this.capsLibrary = {};
    this.lowercaseLibrary = {};
    //setup CAPS sheet
    this.capsSpritesheetURL = "textures/font_boston_spritesheet_caps.png";
    this.capsSpritesheetTex = new THREE.TextureLoader().load(this.capsSpritesheetURL);
    this.capsSpritesheetTex.wrapS = this.capsSpritesheetTex.wrapT = THREE.RepeatWrapping;
    this.capsLibrary[this.capsSpritesheetURL] = this.capsSpritesheetTex;

    //setup lowercase sheet
    this.lowercaseSpritesheetTexURL = "textures/font_boston_spritesheet_lowercase.png";
    this.lowercaseSpritesheetTex = new THREE.TextureLoader().load(this.lowercaseSpritesheetTexURL);
    this.lowercaseSpritesheetTex.wrapS = this.lowercaseSpritesheetTex.wrapT = THREE.RepeatWrapping;
    this.lowercaseLibrary[this.lowercaseSpritesheetTexURL] = this.lowercaseSpritesheetTex;
}

SpriteNodesLetters.prototype.init = function () {
    //create a group of letters
    this.group = new THREE.Group();
    var numParticles = 25;
    for (var i = 0; i < numParticles; i++) {
        // sprites
        var px = 270 * (Math.random() - 0.5);
        var py = 305 * (Math.random() - 0.5);
        var pz = (-105 * (Math.random())) - 200;
        //setup CAPS sprite
        this.sprite1 = new THREE.Sprite(new THREE.SpriteNodeMaterial());
        this.group.add(this.sprite1);
        this.sprite1.scale.x = 10;
        this.sprite1.scale.y = 10;
        this.sprite1.position.x += -25;
        this.sprite1.position.x += px;
        this.sprite1.position.y += py;
        this.sprite1.position.z += pz;
        this.sprite1.material.color = new THREE.TextureNode(this.capsSpritesheetTex);
        // sprite1.material.color.uv = createHorizontalSpriteSheetNode(26, 10);
        this.sprite1.material.color.uv = this.createInputSpriteSheetNode(26, 0);
        this.sprite1.material.alpha = new THREE.Math1Node(new THREE.SwitchNode(this.sprite1.material.color, "r"), THREE.Math1Node.INVERT);
        this.sprite1.material.build();

        //setup lowercase sprite
        this.sprite2 = new THREE.Sprite(new THREE.SpriteNodeMaterial());
        this.group.add(this.sprite2);
        this.sprite2.scale.x = 10;
        this.sprite2.scale.y = 10;
        this.sprite2.position.x += 25;
        this.sprite2.position.x += px;
        this.sprite2.position.y += py;
        this.sprite2.position.z += pz;
        this.sprite2.material.color = new THREE.TextureNode(this.lowercaseSpritesheetTex);
        // sprite2.material.color.uv = createHorizontalSpriteSheetNode(26, 10);
        this.sprite2.material.color.uv = this.createInputSpriteSheetNode(26, 0);
        this.sprite2.material.alpha = new THREE.Math1Node(new THREE.SwitchNode(this.sprite2.material.color, "r"), THREE.Math1Node.INVERT);
        this.sprite2.material.build();
    }
}

SpriteNodesLetters.prototype.getGroup = function(){
    return this.group;
}


// horizontal sprite-sheet animator

SpriteNodesLetters.prototype.createHorizontalSpriteSheetNode = function (hCount, speed) {

    var speed = new THREE.Vector2Node(speed, 0); // frame per second
    var scale = new THREE.Vector2Node(1 / hCount, 1); // 8 horizontal images in sprite-sheet

    var uvTimer = new THREE.OperatorNode(
        new THREE.TimerNode(),
        speed,
        THREE.OperatorNode.MUL
    );

    var uvIntegerTimer = new THREE.Math1Node(
        uvTimer,
        THREE.Math1Node.FLOOR
    );

    var uvFrameOffset = new THREE.OperatorNode(
        uvIntegerTimer,
        scale,
        THREE.OperatorNode.MUL
    );

    var uvScale = new THREE.OperatorNode(
        new THREE.UVNode(),
        scale,
        THREE.OperatorNode.MUL
    );

    var uvFrame = new THREE.OperatorNode(
        uvScale,
        uvFrameOffset,
        THREE.OperatorNode.ADD
    );

    return uvFrame;
}


SpriteNodesLetters.prototype.spriteToJSON = function (sprite, targetTex, targetTexURL, targetLibrary) {

    // serialize

    var json = sprite.material.toJSON();

    // replace uuid to url (facilitates the load of textures using url otherside uuid)
    THREE.NodeMaterialLoaderUtils.replaceUUID(json, targetTex, targetTexURL);

    // deserialize

    var material = new THREE.NodeMaterialLoader(null, targetLibrary).parse(json);

    // replace material

    sprite.material.dispose();
    // sprite1.needsUpdate = true;

    sprite.material = material;

}

SpriteNodesLetters.prototype.TriggerLetter = function (letter) {
    console.log(letter);
    if (letter.length == 1) {
        console.log("group.children.length: " + this.group.children.length);
        for (var i = 0; i < this.group.children.length; i++) {
            console.log(i);
            var alphaIndex = this.getIndexFromKey(letter); // also send osc here
            var ranIndex = this.getRandomInt(26);
            this.group.children[i].material.dispose();
            this.group.children[i].material.color.uv = this.createInputSpriteSheetNode(26, alphaIndex);
            // group.children[i].material.color.uv = createInputSpriteSheetNode(26, ranIndex);

            // sprite2.material.dispose();
            // sprite2.material.color.uv = createInputSpriteSheetNode(26, alphaIndex);

        }
    }

}

SpriteNodesLetters.prototype.RandomLetterWithDelay = function(indx) {
    indx = parseInt(indx);
    setTimeout(function () {    //  call a 3s setTimeout when the loop is called
        this.group.children[indx].material.dispose();
        var alphaIndex = this.getRandomInt(26);
        var ranIndex = this.getRandomInt(26);
        // group.children[indx].material.color.uv = createInputSpriteSheetNode(26, alphaIndex);
        this.group.children[indx].material.color.uv = this.createInputSpriteSheetNode(26, ranIndex);
        // sendOscPhoneme(getLetterFromIndex(indx))
        indx++;                     //  increment the counter
        if (indx < this.group.children.length) {            //  if the counter < 10, call the loop function
            this.RandomLetterWithDelay(indx);             //  ..  again which will trigger another 
        }                        //  ..  setTimeout()
    }, 100)
    this.letterIndex = (this.letterIndex + 1) % 26;
}




SpriteNodesLetters.prototype.getIndexFromKey = function(keyValue) {
    var index;
    switch (keyValue) {
        case 'a':
            index = 25;
            break;
        case 'b':
            index = 24;
            break;
        case 'c':
            index = 23;
            break;
        case 'd':
            index = 22;
            break;
        case 'e':
            index = 21;
            break;
        case 'f':
            index = 20;
            break;
        case 'g':
            index = 19;
            break;
        case 'h':
            index = 18;
            break;
        case 'i':
            index = 17;
            break;
        case 'j':
            index = 16;
            break;
        case 'k':
            index = 15;
            break;
        case 'l':
            index = 14;
            break;
        case 'm':
            index = 13;
            break;
        case 'n':
            index = 12;
            break;
        case 'o':
            index = 11;
            break;
        case 'p':
            index = 10;
            break;
        case 'q':
            index = 9;
            break;
        case 'r':
            index = 8;
            break;
        case 's':
            index = 7;
            break;
        case 't':
            index = 6;
            break;
        case 'u':
            index = 5;
            break;
        case 'v':
            index = 4;
            break;
        case 'w':
            index = 3;
            break;
        case 'x':
            index = 2;
            break;
        case 'y':
            index = 1;
            break;
        case 'z':
            index = 0;
            break;
    };
    return index;
}

SpriteNodesLetters.prototype.getLetterFromIndex = function(alphaIndex) {
    var letter;
    switch (alphaIndex) {
        case 0:
            letter = 'a';
            break;
        case 1:
            letter = 'b';
            break;
        case 2:
            letter = 'c';
            break;
        case 3:
            letter = 'd';
            break;
        case 4:
            letter = 'e';
            break;
        case 5:
            letter = 'f';
            break;
        case 6:
            letter = 'g';
            break;
        case 7:
            letter = 'h';
            break;
        case 8:
            letter = 'i';
            break;
        case 9:
            letter = 'j';
            break;
        case 10:
            letter = 'k';
            break;
        case 11:
            letter = 'l';
            break;
        case 12:
            letter = 'm';
            break;
        case 13:
            letter = 'n';
            break;
        case 14:
            letter = 'o';
            break;
        case 15:
            letter = 'p';
            break;
        case 16:
            letter = 'q';
            break;
        case 17:
            letter = 'r';
            break;
        case 18:
            letter = 's';
            break;
        case 19:
            letter = 't';
            break;
        case 20:
            letter = 'u';
            break;
        case 21:
            letter = 'v';
            break;
        case 22:
            letter = 'w';
            break;
        case 23:
            letter = 'x';
            break;
        case 24:
            letter = 'y';
            break;
        case 25:
            letter = 'z';
            break;
    };
    return letter;
}



SpriteNodesLetters.prototype.createInputSpriteSheetNode = function (hCount, index) {
    var scale = new THREE.Vector2Node(1 / hCount, 1); // 8 horizontal images in sprite-sheet

    var uvFrameOffset = new THREE.OperatorNode(
        new THREE.FloatNode(index),
        scale,
        THREE.OperatorNode.MUL
    );

    var uvScale = new THREE.OperatorNode(
        new THREE.UVNode(),
        scale,
        THREE.OperatorNode.MUL
    );

    var uvFrame = new THREE.OperatorNode(
        uvScale,
        uvFrameOffset,
        THREE.OperatorNode.ADD
    );

    return uvFrame;

}

SpriteNodesLetters.prototype.getRandomInt = function (max) {
    return Math.floor(Math.random() * Math.floor(max));
}



SpriteNodesLetters.prototype.update = function (hCount, speed) {
    var delta = clock.getDelta();

    // update material animation and/or gpu calcs (pre-renderer)
    this.frame.update(delta)
        .updateNode(this.sprite1.material)
        .updateNode(this.sprite2.material)


}

