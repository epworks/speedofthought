(
var b_speech, sd_playSpeech, sd_routeVerb,speechStarts, speechDurs, phonemeList, oscAddress;
var osc;
var phonemeIndex=0;
var reverbRoute;
var pitchMinMidi, pitchMaxMidi, wordDurMidi;
pitchMaxMidi = 2; //CtkControl.play(initVal: 2); // max pitchshift
pitchMinMidi = 0.5 ;//CtkControl.play(initVal: 0.5); // min pitchshift
wordDurMidi = 0.5;

oscAddress = NetAddr("127.0.0.1", 57121); // loopback

reverbRoute = CtkAudio.new(2);

osc =OSCFunc.new({ arg msg, time;
	var splitString;
	phonemeIndex = 0; // reset phoneme index
	"msg[2]: ".post; msg[2].postln;
	speechDurs = msg[1].asString.split($,);
	"speechDurs: ".post;
	//there is some extra value at the end of the osc s
	speechDurs.do({arg i; i.post; " ".post;});
	speechStarts = msg[2].asString.split($,);
	"startTimes: ".post;
	speechStarts.do({arg i; i.post; " ".post;}
	);
	"-".postln;
}, '/WordData');

// b_speech = CtkBuffer.playbuf("/Users/erikparr/Documents/_ElectronApp/speedofthought/data/190106_wordsignal/audio.wav").load(sync: true); // load and sync with the server
// b_speech = CtkBuffer.playbuf("/Users/erikparr/Documents/_ElectronApp/speedofthought/data/solfege/solfege.wav").load(sync: true); // load and sync with the server
b_speech = CtkBuffer.playbuf("/Users/erikparr/Documents/_ElectronApp/speedofthought/data/bodies/secondbody.wav").load(sync: true); // load and sync with the server

b_speech.sampleRate.postln;

sd_playSpeech = CtkSynthDef("sd_playSpeech", {arg bus = 0, buf = 0, dur, startPos, pitch;
	var env, src, ps;
	//make sure
	src = PlayBuf.ar(2, buf, 1, startPos:startPos );
	ps = PitchShift.ar(src,0.2,pitch);
	env = EnvGen.ar(Env([0, 1, 1, 0], [0.01,0.98,0.01], \sin),timeScale: dur, doneAction:2);
	Out.ar(bus, Pan2.ar(env*src))
});

sd_routeVerb = CtkSynthDef("sd_routeVerb", {arg inbus, amp, dur,pan = 0, gate=1;
	var in, filt, env;
	in =  In.ar(inbus);
	filt = GVerb.ar(in,50,1,0.666,1,15,2);
	env = EnvGen.ar(Env([0, 1, 1, 0], [0.1,0.8,0.1], \sin),timeScale: dur, doneAction:2);
	Out.ar(0, Pan2.ar(filt,pan));
}).load(s);




p = ProcMod .new(Env([0, 1, 0], [1,1], \sin, 1), target:1, addAction:\tail)
// the unique Group id created by ProcModR is passed in to this function,
// as well as the unique routing bus and the server, as well as the ProcModR itself
.function_({arg group, routebus, server, pm;
	var wordTask, playTask, cleanPlayTask, sequence;
	var getPhonemeRoutine, globalDur, durMask, phoneme;
	var playnote;
	globalDur = 60;


	//incoming WordData packet contains durationString and sampleString,
	//each contains a list of values separated by ','

	//////////////////
	/////wordTask runs a loop that requests new word data from JS app
	////////////////////////
	wordTask =  Routine({//some args here
		arg i;
		var noteDur, wordDur, modnote, index;
		var wordStart, wordTend, wordloopTime;
		var stateDur=50;
		var sliderA;
		noteDur=1;

		//////////////////
		/////two loops: one for updating word data, a second for playing samples
		////////////////////////

		// looping Tendency mask
		wordloopTime=10;
		wordTend = Pfunc({arg timeIn = 0;
			Tendency.new(
				Env([2,2], [wordloopTime], \sin),
				Env([1,1], [wordloopTime], \sin), defDist:\betaRand).at(timeIn)
		}).asStream;
		wordStart = SystemClock.seconds;
		loop ({
			wordDur = wordTend.next((SystemClock.seconds - wordStart) % 10.0).postln;
			oscAddress.sendMsg("/word", "ok");
			wordDur.wait;
		});
	});


	//////////////////
	/////playTask runs a loop that plays the phonemes
	////////////////////////

	playTask =  Routine({//some args here
		arg i;
		var noteDur, wordDur, playnote, modnote, index;
		var playTend, playStart, playLoopTime, offset;
		var stateDur=50;
		var sliderA;
		phonemeIndex=0;


		playLoopTime=15;
		noteDur = 1;
		playTend = Pfunc({arg timeIn = 0;
			Tendency.new(
				Env([0.15,0.01], [playLoopTime], \sin),
				Env([0.075,0.005], [playLoopTime], \sin), defDist:\betaRand).at(timeIn)
		}).asStream;
		playStart = SystemClock.seconds;
		oscAddress.sendMsg("/word", "ok");
		oscAddress.sendMsg("/word", "ok");
		oscAddress.sendMsg("/word", "ok");
		"hi".postln;

		loop ({
			if(speechDurs.size>0 ){
				if(IsNil.value(speechDurs[phonemeIndex]) == false){
					noteDur = playTend.next((SystemClock.seconds - playStart));
					offset = speechStarts[phonemeIndex].asInt * 0.13;
					playnote = sd_playSpeech.note(target: group, addAction: \addToHead)
					.buf_(b_speech)
					.pitch_(1)
					.dur_(speechDurs[phonemeIndex].asFloat)
					.startPos_(offset+speechStarts[phonemeIndex].asInt)
					.bus_(reverbRoute)
					.play;
					phonemeIndex = (phonemeIndex+1)%speechDurs.size;
					//trigger next word
					if((SystemClock.seconds.asInt - playStart.asInt)%playLoopTime==0){
						oscAddress.sendMsg("/word", "ok");
					}

				};
			};
			noteDur.wait;

		})
	});


	cleanPlayTask =  Routine({
		arg i;
		var noteDur, wordDur, playnote, modnote, index;
		var pitchTend, pitchMod, playStart, tendLoopTime, offset;
		var stateDur=50;
		var sliderA;
		pitchMod=1;
		phonemeIndex=0;
		tendLoopTime=120; // tendency loops for 60 sec
		noteDur = 0.05;
		pitchTend = Pfunc({arg timeIn = 0;
			Tendency.new(
				Env([1.0, 0.5,2.0,4.0], [tendLoopTime*0.333, tendLoopTime*0.333, tendLoopTime*0.333], \sin),
				Env([1.0, 0.5,0.5,4.0], [tendLoopTime*0.333, tendLoopTime*0.333, tendLoopTime*0.333], \sin), defDist:\betaRand).at(timeIn)
		}).asStream;
		playStart = SystemClock.seconds;
		loop ({
			noteDur = wordDurMidi;
			if(speechDurs.size>0 ){
				if(IsNil.value(speechDurs[phonemeIndex]) == false){
					pitchMod = pitchTend.next((SystemClock.seconds - playStart));
					offset = speechStarts[phonemeIndex].asInt * 0.13;
					noteDur = speechDurs[phonemeIndex].asFloat * wordDurMidi;
					playnote = sd_playSpeech.note(target: group, addAction: \addToHead)
					.buf_(b_speech)
					.pitch_(rrand(pitchMinMidi,pitchMaxMidi)) //pitchMod
					.dur_(speechDurs[phonemeIndex].asFloat)
					.startPos_(offset+speechStarts[phonemeIndex].asInt)
					.bus_(reverbRoute)
					.play;
					phonemeIndex = (phonemeIndex+1)%speechDurs.size;
				};
			};
			noteDur.wait;
			oscAddress.sendMsg("/word", "ok");
		})
	});

	/////midi---------------------------------//
	/////-------------------------------------//
	/////-------------------------------------//
	/////-------------------------------------//
	MIDIIn.connectAll;


	//-------------------------------------//
	//--slider 1: pitch min---//

	MIDIdef.cc(\pitchMin, {arg  key, func, ccNum, chan;
		pitchMinMidi = key.linlin(0,128,0.4,1.0); // remap values using linlin!!
		"pitchMin: ".post;
		pitchMinMidi.postln;
		// pitchMinMidi.set(pitchMinRemap(key));
	}, 77); // pitch shift midi ctrl

	//-------------------------------------//
	//--slider 2: pitch max---//

	MIDIdef.cc(\pitchMax, {arg  key, func, ccNum, chan;
		pitchMaxMidi =  key.linlin(0,128,1.0,4.5);
		"pitchMax: ".post;
		pitchMaxMidi.postln;
		// pitchMaxMidi.set(pitchMaxRemap(key));
	}, 78); // pitch shift midi ctrl

	//-------------------------------------//
	//--slider 2: pitch max---//

	MIDIdef.cc(\wordDur, {arg  key, func, ccNum, chan;
		wordDurMidi =  key.linlin(0,128,0.075,2);
		"wordDur: ".post;
		wordDurMidi.postln;
		// pitchMaxMidi.set(pitchMaxRemap(key));
	}, 79); // pitch shift midi ctrl


	MIDIdef.noteOn(\someKeyboard, { arg key, func, noteNum, chan, srcID;
		func.postln;
		//-------------------------------------//
		//--button 1 Top and Bottom //
		(func==41).if{
			playTask.reset;
			playTask.play;
			cleanPlayTask.stop;

		};
		(func==73).if{
			playTask.stop;
		};
		//-------------------------------------//
		//--button 2 Top and Bottom //
		(func==42).if{
			cleanPlayTask.reset;
			cleanPlayTask.play;
			playTask.stop;

		};
		(func==74).if{
			cleanPlayTask.stop;
		};

	});


	/////----------------------------

	sequence = Task({
		var reverbNote;
		var taskdur = 400;

		reverbNote = sd_routeVerb.note(target: group, addAction: \addToTail)
		.inbus_(reverbRoute)
		.amp_(1)
		.dur_(taskdur)
		.play;

		//sequence controls the playback of all the routines
		// playTask.play;
		// cleanPlayTask.play;
		// taskdur.wait;
		// playTask.stop;

	});
	// wordTask.play;
	// cleanPlayTask.play;

	sequence.play
});

)

p.play
p.release
