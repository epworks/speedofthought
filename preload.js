var { webFrame } = require('electron')

process.once('loaded', function() {
  // Allow window.fetch() to access app files
  webFrame.registerUrlSchemeAsBypassingCsp("app", {
    webSecurity: true,
    bypassCSP: false,
    allowRunningInsecureContent: true,
    allowDisplayingInsecureContent: true,
    allowServiceWorkers: true,
    supportFetchAPI: true,
    corsEnabled: false
  });
});